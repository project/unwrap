INSTALL
=======
1) Untar and unpack the unwrap module and its files into the sites/all/modules directory
2) Enable module
3) Enable the input filter for the various input formats you wish to use it for

Credit: Written by Steve Dondley, Owner, Prometheus Labor Communications, Inc.
